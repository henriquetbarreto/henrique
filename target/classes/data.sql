insert into product (name, description, parent_product_id) values ('Produto 1', 'Descrição do Produto 1', null);
insert into product (name, description, parent_product_id) values ('Produto 2', 'Descrição do Produto 2', null);
insert into product (name, description, parent_product_id) values ('Produto 3 - Filho do produto 1', 'Descrição do Produto 3', 1);
insert into product (name, description, parent_product_id) values ('Produto 4 - Filho do produto 1', 'Descrição do Produto 4', 1);
insert into product (name, description, parent_product_id) values ('Produto 5 - Filho do produto 2', 'Descrição do Produto 5', 2);

insert into image (product_id) values (1);
insert into image (product_id) values (1);
insert into image (product_id) values (2);
insert into image (product_id) values (2);
insert into image (product_id) values (3);
