package home.henrique.product.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.dom4j.tree.AbstractEntity;

@Entity
@Table(name = "Image")
public class Image extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8131158664327749534L;

	@Id
	@GeneratedValue
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id")
	private Product joinImage;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
