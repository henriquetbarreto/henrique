package home.henrique.product.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.dom4j.tree.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Product")
public class Product extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5540525227541921553L;
	
	public Product() {
		
	}
	
	public Product (String name, String description,  Long parentProductId) {
		this.name = name;
		this.description = description;
		this.setParentProductId(parentProductId);
	}

	@Id
	@GeneratedValue
	private int id;

	private String name;
	private String description;
	
	@OneToMany(mappedBy="childProduct")
	private Set<Product> childrenProduct;
	
	@ManyToOne()
	@JoinColumn(name = "parentProductId", nullable = true)
    private Product childProduct;
	
	@OneToMany(mappedBy="joinImage")
	private Set<Image> images;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setParentProductId(Long parentProductId) {
	}

	public Product getChildProduct() {
		return childProduct;
	}

	public void setChildProduct(Product childProduct) {
		this.childProduct = childProduct;
	}

	@JsonIgnore
	public Set<Product> getChildrenProduct() {
		return childrenProduct;
	}

	public void setChildrenProduct(Set<Product> childrenProduct) {
		this.childrenProduct = childrenProduct;
	}

	
	public Set<Image> getImages() {
		return images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}
}
