package home.henrique.product.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import home.henrique.product.application.controller.ProductController;

public class ProductConfig {

	@Context
    UriInfo uriInfo;
	
	@Configuration
	@ApplicationPath("/jersey")
	public class JerseyConfig extends ResourceConfig {
	    public JerseyConfig() {
	        register(ProductController.class);
	    }
	}
}
