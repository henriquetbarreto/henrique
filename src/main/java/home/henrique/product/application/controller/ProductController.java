package home.henrique.product.application.controller;

import java.net.URI;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import home.henrique.product.application.repository.ProductRepository;
import home.henrique.product.model.Image;
import home.henrique.product.model.Product;

@Component
@RestController
@Path("product")
@Produces(MediaType.APPLICATION_JSON)
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;

	@POST
	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public Response save(Product product) {
		
		product = productRepository.save(product);
		
		URI location = UriBuilder.fromResource(ProductController.class)
				.path("{id}")
				.resolveTemplate("id", product.getId())
				.build();
		
		return Response.created(location).build();
	}
	
	@DELETE
	@RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
	public Response delete(@PathVariable("id") final Integer id) {
		
		productRepository.delete(id);
	    return Response.accepted().build();
	}
	
	@GET
	@Produces("application/json")
	@RequestMapping(value = "/product/no/relationship", method = RequestMethod.GET)
	public Iterable<Product> findAllExcludingRelationships() {

		return productRepository.findAllExcludingRelationships();
	}
	
	@GET
	@Produces("application/json")
	@RequestMapping(value = "/product/no/relationship/{id}", method = RequestMethod.GET)
	public Object findByIdExcludingRelationships(@PathVariable("id") final Integer id) {

		return productRepository.findByIdExcludingRelationships(id);
	}
	
	@GET
	@Produces("application/json")
	@RequestMapping(value = "/product/with/relationship", method = RequestMethod.GET)
	public Iterable<Product> findAllWithRelationships() {

		return productRepository.findAll();
	}
	
	@GET
	@RequestMapping(value = "/product/with/relationship/{id}", method = RequestMethod.GET)
	public Product getById(@PathVariable("id") final Integer id) {
		
		return productRepository.findOne(id);
	}
	
	@GET
	@RequestMapping(value = "/product/children/{id}", method = RequestMethod.GET)
	public Set<Product> getProductChildrenById(@PathVariable("id") final Integer id) {
		
		Product product = productRepository.findOne(id);
		return product.getChildrenProduct();
	}
	
	@GET
	@RequestMapping(value = "/product/images/{id}", method = RequestMethod.GET)
	public Set<Image> getImagesById(@PathVariable("id") final Integer id) {
		
		Product product = productRepository.findOne(id);
		return product.getImages();
	}
}
