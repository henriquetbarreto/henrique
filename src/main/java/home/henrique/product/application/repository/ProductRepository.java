package home.henrique.product.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import home.henrique.product.model.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>{

	 @Query("SELECT name, description FROM Product")
	 public List<Product> findAllExcludingRelationships();
	 
	 @Query("SELECT id, name, description FROM Product p where p.id = :id")
	 public Object findByIdExcludingRelationships(@Param("id") Integer id);
	 
	 /*@Query("SELECT name, description, parentProduct, images FROM Product ")*/
	 /*public List<Product> findAllWithRelationships();*/
}
