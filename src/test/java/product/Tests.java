package product;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import home.henrique.product.application.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class Tests {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void product() throws Exception {
		mockMvc.perform(get("http://localhost:8080/product/no/relationship/1")).andExpect(status().isOk())
				.andExpect(content().json("[[1, 'Produto 1', 'Descrição do Produto 1' ]]"));
	}

	@Test
	public void testGetAll() throws Exception {
		mockMvc.perform(get("/product/no/relationship/1")).andExpect(status().isOk()).andExpect(content().json(
				"[[Produto 1,Descrição do Produto 1],[Produto 2,Descrição do Produto 2],[Produto 3 - Filho do produto 1,Descrição do Produto 3],[Produto 4 - Filho do produto 1,Descrição do Produto 4],[Produto 5 - Filho do produto 2,Descrição do Produto 5]]"));
	}
}
