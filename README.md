# README #
**Queries ####** 

* Get all products without relationships: GET - localhost:8080/product/no/relationship 
* Get all products with relationships: GET - localhost:8080/product/with/relationship 
* Get product by id without relationships: GET - localhost:8080/product/no/relationship/{id} 
* Get product by id with relationships: GET - localhost:8080/product/with/relationship/{id} 
* Get product children by product id: GET - localhost:8080/product/children/{id} 
* Get product images by product id: GET - localhost:8080/product/images/{id}

**TO insert new Product**: * POST - localhost:8080/product
Needs to pass by parameters name, description, and if needed parent_product_id

**To Delete a Product**: * DELETE - localhost:8080/product/{id}


By Henrique Barreto